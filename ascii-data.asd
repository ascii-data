;;;; ascii-data.asd

(asdf:defsystem #:ascii-data
  :name "ascii-data"
  :description "Read numerical data from an ascii text file into an array and vice versa."
  :license "Public Domain"
  :author "Sumant Oemrawsingh"
  :depends-on (#:parse-float)
  :components ((:file "package")
               (:file "ascii-data"
		      :depends-on ("package"))))
