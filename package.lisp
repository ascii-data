;;;; package.lisp

(defpackage #:ascii-data
  (:use #:cl)
  (:export #:read-ascii-array*
	   #:read-ascii-array

	   #:read-ascii-vector-or-array*
	   #:read-ascii-vector-or-array

	   #:write-ascii-array*
	   #:write-ascii-array

	   #:write-ascii-vector-or-array*
	   #:write-ascii-vector-or-array))


